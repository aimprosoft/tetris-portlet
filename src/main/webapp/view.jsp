<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />

<script type="text/javascript">
    jQuery(document).ready(function(){
        
        new TetrisComponent({
            namespace: '<portlet:namespace/>',
            containerId: 'gameField'
        });
        
        jQuery("#" + "<portlet:namespace/>gameField").focus();
        
    });
</script>

<div class="tetris-main-content">
    
    <div class="message-content">
        <div id="<portlet:namespace/>message-box">
        </div>
    </div>

    <div class="help-content">

        <div>
            <b>A</b> - left
        </div>
        <div>
            <b>D</b> - right
        </div>
        <div>
            <b>S</b> - down
        </div>
        <div>
            <b>W</b> - rotate
        </div>
        <div>
            <b>Space</b> - pause
        </div>

    </div>
    
    <div id="<portlet:namespace/>gameField" class="gameField" tabindex="0">
        <%-- Rendered by JS --%>
    </div>

    
    
</div>     