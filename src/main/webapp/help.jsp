<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />


<div class="tetris-main-content">

    <div class="help-content">

        <div>
            <b>A</b> - left
        </div>
        <div>
            <b>D</b> - right
        </div>
        <div>
            <b>S</b> - down
        </div>
        <div>
            <b>W</b> - rotate
        </div>
        <div>
            <b>Space</b> - pause
        </div>
        
    </div>
    
</div>     