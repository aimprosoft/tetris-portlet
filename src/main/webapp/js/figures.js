var EMPTY = 0;
var FILLED = 1;

/**
 * Base Figure Class
 */
var BaseFigure = function () {

    /**
     * Checks if figure can be rendered.
     * Invoked first time, figure is created.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {boolean} - true, if figure can be rendered
     */
    this.canRender = function (cells, row, column) {
        return false;
    };


    /**
     * Renders figure.
     * Invoked first time, figure is created.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {*} - modified array
     */
    this.render = function (cells, row, column) {
        return cells;
    };

    /**
     * Checks if figure can be moved down.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {boolean} - true, if figure can be moved down
     */
    this.canMoveDown = function (cells, row, column) { //overwritten in child classes
        return false;
    };

    /**
     * Moves figure one cell down.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {*} - modified array
     */
    this.moveDown = function (cells, row, column) {
        return cells;
    };

    /**
     * Checks if figure can be moved left.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {boolean} - true, if figure can be moved left
     */
    this.canMoveLeft = function (cells, row, column) {
        return false;
    };

    /**
     * Moves figure one cell left.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {*} - modified array
     */
    this.moveLeft = function (cells, row, column) {
        return cells;
    };

    /**
     * Checks if figure can be moved right.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {boolean} - true, if figure can be moved right
     */
    this.canMoveRight = function (cells, row, column) {
        return false;
    };

    /**
     * Moves figure one cell right.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {*} - modified array
     */
    this.moveRight = function (cells, row, column) {
        return cells;
    };

    /**
     * Checks if figure can be rotated.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {boolean} - true, if figure can be rotated
     */
    this.canRotate = function (cells, row, column) {
        return false;
    };

    /**
     * Rotates figure.
     * Overwritten in child classes.
     *
     * @param cells  - array of cells
     * @param row    - current row index
     * @param column - current column index
     *
     * @return {*} - modified array
     */
    this.rotate = function (cells, row, column) {
        return cells;
    };

    /**
     * Validates indexes to avoid ArrayIndexOfBound
     *
     * @param cells  - array of cells
     * @param row    - row index to check
     * @param column - column index to check
     *
     * @return {boolean} - true, if indexes are valid
     */
    this.indexesValid = function (cells, row, column) {
        var rows = cells.length;
        var columns = cells[0].length;
        return row >= 0 && column >= 0 && row < rows && column < columns;
    }
};
/**
 * BaseFigure object
 * @type {BaseFigure}
 */
var baseFigure = new BaseFigure();


var Figure0 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row + 1, column) &&
            this.indexesValid(cells,row, column + 1) &&
            this.indexesValid(cells,row + 1, column + 1)) {

            return  cells[row][column] == EMPTY &&
                    cells[row + 1][column] == EMPTY &&
                    cells[row][column + 1] == EMPTY &&
                    cells[row + 1][column + 1] == EMPTY;
        }
        return false;
    };

    this.render = function(cells, row, column) {
        cells[row][column] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row][column + 1] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells,row,column){

        if (this.indexesValid(cells,row + 2, column) &&
            this.indexesValid(cells,row + 2, column + 1)) {

            return  cells[row + 2][column] == EMPTY &&
                    cells[row + 2][column + 1] == EMPTY;

        }
        return false;
    };

    this.moveDown = function(cells,row,column){
        cells[row][column] = EMPTY;
        cells[row][column + 1] = EMPTY;
        cells[row + 2][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells,row,column) {
        if (this.indexesValid(cells,row, column - 1) &&
            this.indexesValid(cells,row + 1, column - 1)) {

            return  cells[row][column - 1] == EMPTY &&
                    cells[row + 1][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells,row,column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells,row,column) {
        if (this.indexesValid(cells,row, column + 2) &&
            this.indexesValid(cells,row + 1, column + 2)) {

            return  cells[row][column + 2] == EMPTY &&
                    cells[row + 1][column + 2] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells,row,column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canRotate = function(cells,row,column) {
        return false;
    };

    this.rotate = function(cells,row,column) {
        return cells;
    }
};


var Figure1 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row, column + 1) &&
            this.indexesValid(cells,row, column + 2) &&
            this.indexesValid(cells,row, column + 3)) {

            return  cells[row][column] == EMPTY &&
                    cells[row][column + 1] == EMPTY &&
                    cells[row][column + 2] == EMPTY &&
                    cells[row][column + 3] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column] = FILLED;
        cells[row][column + 1] = FILLED;
        cells[row][column + 2] = FILLED;
        cells[row][column + 3] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells,row,column){

        if (this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 1, column + 1)&&
            this.indexesValid(cells, row + 1, column + 2)&&
            this.indexesValid(cells, row + 1, column + 3)) {

            return  cells[row + 1][column] == EMPTY &&
                    cells[row + 1][column + 1] == EMPTY &&
                    cells[row + 1][column + 2] == EMPTY &&
                    cells[row + 1][column + 3] == EMPTY;

        }
        return false;

    };

    this.moveDown = function(cells,row,column) {
        cells[row][column] = EMPTY;
        cells[row][column + 1] = EMPTY;
        cells[row][column + 2] = EMPTY;
        cells[row][column + 3] = EMPTY;
        cells[row + 1][column] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        cells[row + 1][column + 3] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells,row,column) {
        if (this.indexesValid(cells, row, column - 1)) {
            return cells[row][column - 1] == EMPTY;
        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 3] = EMPTY;
        cells[row][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells,row,column) {
        if (this.indexesValid(cells, row, column + 4)) {
            return cells[row][column + 4] == EMPTY;
        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 4] = FILLED;
        return cells;
    };


    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 2, column)&&
            this.indexesValid(cells, row + 3, column)) {
            return cells[row + 1][column] == EMPTY &&
                   cells[row + 2][column] == EMPTY &&
                   cells[row + 3][column] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row][column + 2] = EMPTY;
        cells[row][column + 3] = EMPTY;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column] = FILLED;
        cells[row + 3][column] = FILLED;
        return cells;
    };

};

var Figure2 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row + 1, column) &&
            this.indexesValid(cells,row + 2, column) &&
            this.indexesValid(cells,row + 3, column)) {

            return  cells[row][column] == EMPTY &&
                    cells[row + 1][column] == EMPTY &&
                    cells[row + 2][column] == EMPTY &&
                    cells[row + 3][column] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column] = FILLED;
        cells[row + 3][column] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 4, column)) {
            return cells[row + 4][column] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 4][column] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column - 1) &&
            this.indexesValid(cells, row + 2, column - 1) &&
            this.indexesValid(cells, row + 3, column - 1)) {

            return cells[row][column - 1] == EMPTY &&
                   cells[row + 1][column - 1] == EMPTY &&
                   cells[row + 2][column - 1] == EMPTY &&
                   cells[row + 3][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row + 3][column] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        cells[row + 2][column - 1] = FILLED;
        cells[row + 3][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 2, column + 1) &&
            this.indexesValid(cells, row + 3, column + 1)) {

            return cells[row][column + 1] == EMPTY &&
                cells[row + 1][column + 1] == EMPTY &&
                cells[row + 2][column + 1] == EMPTY &&
                cells[row + 3][column + 1] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row + 3][column] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        cells[row + 3][column + 1] = FILLED;
        return cells;
    };


    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row, column + 2)&&
            this.indexesValid(cells, row, column + 3)) {
            return cells[row][column + 1] == EMPTY &&
                   cells[row][column + 2] == EMPTY &&
                   cells[row][column + 3] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row + 3][column] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row][column + 2] = FILLED;
        cells[row][column + 3] = FILLED;
        return cells;
    };

};


var Figure3 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row + 1, column) &&
            this.indexesValid(cells,row + 2, column) &&
            this.indexesValid(cells,row + 2, column + 1)) {

            return  cells[row][column] == EMPTY &&
                    cells[row + 1][column] == EMPTY &&
                    cells[row + 2][column] == EMPTY &&
                    cells[row + 2][column + 1] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column]     = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 3, column) &&
            this.indexesValid(cells, row + 3, column + 1)) {

            return cells[row + 3][column] == EMPTY &&
                   cells[row + 3][column + 1] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row + 3][column] = FILLED;
        cells[row + 3][column + 1] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column - 1) &&
            this.indexesValid(cells, row + 2, column - 1)) {

            return cells[row][column - 1] == EMPTY &&
                cells[row + 1][column - 1] == EMPTY &&
                cells[row + 2][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        cells[row + 2][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 2, column + 2)) {

            return cells[row][column + 1] == EMPTY &&
                cells[row + 1][column + 1] == EMPTY &&
                cells[row + 2][column + 2] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 2] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row, column + 2)) {
            return cells[row][column + 1] == EMPTY &&
                cells[row][column + 2] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row + 2][column] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row][column + 2] = FILLED;
        return cells;
    };
};


var Figure4 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row + 1, column) &&
            this.indexesValid(cells,row, column + 1) &&
            this.indexesValid(cells,row, column + 2)) {

            return  cells[row][column] == EMPTY &&
                cells[row + 1][column] == EMPTY &&
                cells[row][column + 1] == EMPTY &&
                cells[row][column + 2] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column]     = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row][column + 1] = FILLED;
        cells[row][column + 2] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 2, column) &&
            this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 1, column + 2)) {

            return cells[row + 2][column] == EMPTY &&
                   cells[row + 1][column + 1] == EMPTY &&
                   cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 1] = EMPTY;
        cells[row][column + 2] = EMPTY;
        cells[row + 2][column] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column - 1)) {

            return cells[row][column - 1] == EMPTY &&
                   cells[row + 1][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 3) &&
            this.indexesValid(cells, row + 1, column + 1)) {

            return cells[row][column + 3] == EMPTY &&
                   cells[row + 1][column + 1] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row][column + 3] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 2, column + 1)) {
            return cells[row + 1][column + 1] == EMPTY &&
                   cells[row + 2][column + 1] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };
};

var Figure5 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row, column + 1) &&
            this.indexesValid(cells,row + 1, column + 1) &&
            this.indexesValid(cells,row + 2, column + 1)) {

            return cells[row][column] == EMPTY &&
                   cells[row][column + 1] == EMPTY &&
                   cells[row + 1][column + 1] == EMPTY &&
                   cells[row + 2][column + 1] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column]     = FILLED;
        cells[row][column + 1] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 3, column + 1)) {

            return cells[row + 1][column] == EMPTY &&
                cells[row + 3][column + 1] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column] = FILLED;
        cells[row + 3][column + 1] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 2, column)) {

            return cells[row][column - 1] == EMPTY &&
                   cells[row + 1][column] == EMPTY &&
                   cells[row + 2][column] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 2) &&
            this.indexesValid(cells, row + 1, column + 2) &&
            this.indexesValid(cells, row + 2, column + 2)) {

            return cells[row][column + 2] == EMPTY &&
                   cells[row + 1][column + 2] == EMPTY &&
                   cells[row + 2][column + 2] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        cells[row + 2][column + 2] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row, column + 2) &&
            this.indexesValid(cells, row + 1, column + 2)) {
            return cells[row + 1][column] == EMPTY &&
                   cells[row][column + 2] == EMPTY &&
                   cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 1] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row + 1][column] = FILLED;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

};

var Figure6 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row + 1, column) &&
            this.indexesValid(cells,row + 1, column + 1) &&
            this.indexesValid(cells,row, column + 2) &&
            this.indexesValid(cells,row + 1, column + 2)) {

            return cells[row + 1][column] == EMPTY &&
                   cells[row + 1][column + 1] == EMPTY &&
                   cells[row][column + 2] == EMPTY &&
                   cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row + 1][column]     = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 2, column) &&
            this.indexesValid(cells, row + 2, column + 1) &&
            this.indexesValid(cells, row + 2, column + 2)) {

            return cells[row + 2][column] == EMPTY &&
                cells[row + 2][column + 1] == EMPTY &&
                cells[row + 2][column + 2] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 2][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        cells[row + 2][column + 2] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row + 1, column - 1)) {

            return cells[row][column + 1] == EMPTY &&
                cells[row + 1][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 3) &&
            this.indexesValid(cells, row + 1, column + 3)) {

            return cells[row][column + 3] == EMPTY &&
                cells[row + 1][column + 3] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row][column + 3] = FILLED;
        cells[row + 1][column + 3] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row, column) &&
            this.indexesValid(cells, row + 2, column) &&
            this.indexesValid(cells, row + 2, column + 1)) {
            return cells[row][column] == EMPTY &&
                   cells[row + 2][column] == EMPTY &&
                   cells[row + 2][column + 1] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row + 1][column + 1] = EMPTY;
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row][column] = FILLED;
        cells[row + 2][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };

};


var Figure7 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column + 1) &&
            this.indexesValid(cells,row + 1, column + 1) &&
            this.indexesValid(cells,row + 2, column + 1) &&
            this.indexesValid(cells,row + 2, column)) {

            return cells[row][column + 1] == EMPTY &&
                   cells[row + 1][column + 1] == EMPTY &&
                   cells[row + 2][column + 1] == EMPTY &&
                   cells[row + 2][column] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column + 1]     = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        cells[row + 2][column] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 3, column) &&
            this.indexesValid(cells, row + 3, column + 1)) {

            return cells[row + 3][column] == EMPTY &&
                   cells[row + 3][column + 1] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row + 3][column] = FILLED;
        cells[row + 3][column + 1] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column) &&
            this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 2, column - 1)) {

            return cells[row][column] == EMPTY &&
                cells[row + 1][column] == EMPTY &&
                cells[row + 2][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row][column] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 2) &&
            this.indexesValid(cells, row + 1, column + 2) &&
            this.indexesValid(cells, row + 2, column + 2)) {

            return cells[row][column + 2] == EMPTY &&
                cells[row + 1][column + 2] == EMPTY &&
                cells[row + 2][column + 2] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        cells[row + 2][column + 2] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row, column) &&
            this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 1, column + 2)) {
            return cells[row][column] == EMPTY &&
                   cells[row + 1][column] == EMPTY &&
                   cells[row][column + 2] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row][column] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };
};

var Figure8 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row + 1, column) &&
            this.indexesValid(cells,row + 1, column + 1) &&
            this.indexesValid(cells,row + 1, column + 2)) {

            return cells[row][column] == EMPTY &&
                cells[row + 1][column] == EMPTY &&
                cells[row + 1][column + 1] == EMPTY &&
                cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 2, column) &&
            this.indexesValid(cells, row + 2, column + 1) &&
            this.indexesValid(cells, row + 2, column + 2)) {

            return cells[row + 2][column] == EMPTY &&
                   cells[row + 2][column + 1] == EMPTY &&
                   cells[row + 2][column + 2] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row + 2][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        cells[row + 2][column + 2] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column - 1)) {

            return cells[row][column - 1] == EMPTY &&
                   cells[row + 1][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row + 1, column + 3)) {

            return cells[row][column + 1] == EMPTY &&
                   cells[row + 1][column + 3] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row + 1][column + 3] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row + 2, column)) {
            return cells[row][column + 1] == EMPTY &&
                   cells[row + 2][column] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row + 2][column] = FILLED;
        return cells;
    };

};


var Figure9 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row + 1, column) &&
            this.indexesValid(cells,row + 2, column) &&
            this.indexesValid(cells,row, column + 1)) {

            return  cells[row][column] == EMPTY &&
                cells[row + 1][column] == EMPTY &&
                cells[row + 2][column] == EMPTY &&
                cells[row][column + 1] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column]     = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column] = FILLED;
        cells[row][column + 1] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 3, column) &&
            this.indexesValid(cells, row + 1, column + 1)) {

            return cells[row + 3][column] == EMPTY &&
                cells[row + 1][column + 1] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 1] = EMPTY;
        cells[row + 3][column] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column - 1) &&
            this.indexesValid(cells, row + 2, column - 1)) {

            return cells[row][column - 1] == EMPTY &&
                cells[row + 1][column - 1] == EMPTY &&
                cells[row + 2][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        cells[row + 2][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 2) &&
            this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 2, column + 1)) {

            return cells[row][column + 2] == EMPTY &&
                   cells[row + 1][column + 1] == EMPTY &&
                   cells[row + 2][column + 1] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row, column + 2) &&
            this.indexesValid(cells, row + 1, column + 2)) {
            return cells[row][column + 2] == EMPTY &&
                   cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };
};

var Figure10 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells,row, column) &&
            this.indexesValid(cells,row, column + 1) &&
            this.indexesValid(cells,row, column + 2) &&
            this.indexesValid(cells,row + 1, column + 2)) {

            return  cells[row][column] == EMPTY &&
                cells[row][column + 1] == EMPTY &&
                cells[row][column + 2] == EMPTY &&
                cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column]     = FILLED;
        cells[row][column + 1] = FILLED;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 2, column + 2)) {

            return cells[row + 1][column] == EMPTY &&
                cells[row + 1][column + 1] == EMPTY &&
                cells[row + 2][column + 2] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 1] = EMPTY;
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 2] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column + 1)) {

            return cells[row][column - 1] == EMPTY &&
                cells[row + 1][column + 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 3) &&
            this.indexesValid(cells, row + 1, column + 3)) {

            return cells[row][column + 3] == EMPTY &&
                cells[row + 1][column + 3] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row][column + 3] = FILLED;
        cells[row + 1][column + 3] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 2, column + 1) &&
            this.indexesValid(cells, row + 2, column)) {
            return cells[row + 1][column + 1] == EMPTY &&
                   cells[row + 2][column + 1] == EMPTY &&
                   cells[row + 2][column] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        cells[row + 2][column] = FILLED;
        return cells;
    };
};


var Figure11 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 1, column + 2)) {

            return  cells[row][column + 1] == EMPTY &&
                cells[row + 1][column] == EMPTY &&
                cells[row + 1][column + 1] == EMPTY &&
                cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column + 1] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 2, column) &&
            this.indexesValid(cells, row + 2, column + 1) &&
            this.indexesValid(cells, row + 2, column + 2)) {

            return cells[row + 2][column] == EMPTY &&
                cells[row + 2][column + 1] == EMPTY &&
                cells[row + 2][column + 2] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row + 2][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        cells[row + 2][column + 2] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column) &&
            this.indexesValid(cells, row + 1, column - 1)) {

            return cells[row][column] == EMPTY &&
                cells[row + 1][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row][column] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 2) &&
            this.indexesValid(cells, row + 1, column + 3)) {

            return cells[row][column + 2] == EMPTY &&
                cells[row + 1][column + 3] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 3] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row, column) &&
            this.indexesValid(cells, row + 2, column)) {
            return cells[row][column] == EMPTY &&
                cells[row + 2][column] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column + 2] = EMPTY;
        cells[row][column] = FILLED;
        cells[row + 2][column] = FILLED;
        return cells;
    };
};

var Figure12 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells, row, column) &&
            this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 2, column) &&
            this.indexesValid(cells, row + 1, column + 1)) {

            return  cells[row][column] == EMPTY &&
                    cells[row + 1][column] == EMPTY &&
                    cells[row + 2][column] == EMPTY &&
                    cells[row + 1][column + 1] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 3, column) &&
            this.indexesValid(cells, row + 2, column + 1)) {

            return  cells[row + 3][column] == EMPTY &&
                    cells[row + 2][column + 1] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 3][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column - 1) &&
            this.indexesValid(cells, row + 2, column - 1)) {

            return  cells[row][column - 1] == EMPTY &&
                    cells[row + 1][column - 1] == EMPTY &&
                    cells[row + 2][column - 1] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        cells[row + 2][column - 1] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row + 1, column + 2) &&
            this.indexesValid(cells, row + 2, column + 1)) {

            return  cells[row][column + 1] == EMPTY &&
                    cells[row + 1][column + 2] == EMPTY &&
                    cells[row + 2][column + 1] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row, column + 2)) {
            return  cells[row][column + 1] == EMPTY &&
                    cells[row][column + 2] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = EMPTY;
        cells[row][column + 1] = FILLED;
        cells[row][column + 2] = FILLED;
        return cells;
    };
};

var Figure13 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells, row, column) &&
            this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row, column + 2) &&
            this.indexesValid(cells, row + 1, column + 1)) {

            return  cells[row][column] == EMPTY &&
                    cells[row][column + 1] == EMPTY &&
                    cells[row][column + 2] == EMPTY &&
                    cells[row + 1][column + 1] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column] = FILLED;
        cells[row][column + 1] = FILLED;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 2, column + 1) &&
            this.indexesValid(cells, row + 1, column + 2)) {

            return  cells[row + 1][column] == EMPTY &&
                    cells[row + 2][column + 1] == EMPTY &&
                    cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 1] = EMPTY;
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column - 1) &&
            this.indexesValid(cells, row + 1, column)) {

            return  cells[row][column - 1] == EMPTY &&
                    cells[row + 1][column] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row][column - 1] = FILLED;
        cells[row + 1][column] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 3) &&
            this.indexesValid(cells, row + 1, column + 2)) {

            return  cells[row][column + 3] == EMPTY &&
                    cells[row + 1][column + 2] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row][column + 3] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 2, column + 1)) {
            return  cells[row + 1][column] == EMPTY &&
                    cells[row + 2][column + 1] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row][column] = EMPTY;
        cells[row][column + 2] = EMPTY;
        cells[row + 1][column] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };
};

var Figure14 = function() {
    jQuery.extend(this, baseFigure);

    this.canRender = function(cells, row, column) {
        if (this.indexesValid(cells, row, column + 1) &&
            this.indexesValid(cells, row + 1, column) &&
            this.indexesValid(cells, row + 1, column + 1) &&
            this.indexesValid(cells, row + 2, column + 1)) {

            return  cells[row][column + 1] == EMPTY &&
                    cells[row + 1][column] == EMPTY &&
                    cells[row + 1][column + 1] == EMPTY &&
                    cells[row + 2][column + 1] == EMPTY;
        }
        return false;
    };

    this.render = function (cells, row, column) {
        cells[row][column + 1] = FILLED;
        cells[row + 1][column] = FILLED;
        cells[row + 1][column + 1] = FILLED;
        cells[row + 2][column + 1] = FILLED;
        return cells;
    };

    this.canMoveDown = function(cells, row, column){
        if (this.indexesValid(cells, row + 2, column) &&
            this.indexesValid(cells, row + 3, column + 1)) {

            return  cells[row + 2][column] == EMPTY &&
                    cells[row + 3][column + 1] == EMPTY;
        }
        return false;
    };

    this.moveDown = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column] = FILLED;
        cells[row + 3][column + 1] = FILLED;
        return cells;
    };

    this.canMoveLeft = function(cells, row, column){
        if (this.indexesValid(cells, row, column) &&
            this.indexesValid(cells, row + 1, column - 1) &&
            this.indexesValid(cells, row + 2, column)) {

            return  cells[row][column] == EMPTY &&
                    cells[row + 1][column - 1] == EMPTY &&
                    cells[row + 2][column] == EMPTY;

        }
        return false;
    };

    this.moveLeft = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column + 1] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row][column] = FILLED;
        cells[row + 1][column - 1] = FILLED;
        cells[row + 2][column] = FILLED;
        return cells;
    };

    this.canMoveRight = function(cells, row, column){
        if (this.indexesValid(cells, row, column + 2) &&
            this.indexesValid(cells, row + 1, column + 2) &&
            this.indexesValid(cells, row + 2, column + 2)) {

            return  cells[row][column + 2] == EMPTY &&
                    cells[row + 1][column + 2] == EMPTY &&
                    cells[row + 2][column + 2] == EMPTY;

        }
        return false;
    };

    this.moveRight = function(cells, row, column) {
        cells[row][column + 1] = EMPTY;
        cells[row + 1][column] = EMPTY;
        cells[row + 2][column + 1] = EMPTY;
        cells[row][column + 2] = FILLED;
        cells[row + 1][column + 2] = FILLED;
        cells[row + 2][column + 2] = FILLED;
        return cells;
    };

    this.canRotate = function(cells, row, column) {
        if (this.indexesValid(cells, row + 1, column + 2)) {
            return  cells[row + 1][column + 2] == EMPTY;
        }
        return false;
    };

    this.rotate = function(cells, row, column) {
        cells[row + 2][column + 1] = EMPTY;
        cells[row + 1][column + 2] = FILLED;
        return cells;
    };
};


var figures = [];
figures.push(new Figure0());
figures.push(new Figure1());
figures.push(new Figure2());
figures.push(new Figure3());
figures.push(new Figure4());
figures.push(new Figure5());
figures.push(new Figure6());
figures.push(new Figure7());
figures.push(new Figure8());
figures.push(new Figure9());
figures.push(new Figure10());
figures.push(new Figure11());
figures.push(new Figure12());
figures.push(new Figure13());
figures.push(new Figure14());
window.figures = figures;