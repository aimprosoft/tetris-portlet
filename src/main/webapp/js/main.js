var TetrisComponent = function(config) {

    this.namespace = config.namespace;
    
    if (window.tetrisScope === undefined) {
        window.tetrisScope = {};
    }

    window.tetrisScope[this.namespace] = this;

    window.tetrisScope[this.namespace].containerId = config.containerId;
    
    window.tetrisScope[this.namespace].container = jQuery("#" + this.namespace + config.containerId);

    //count of rows
    window.tetrisScope[this.namespace].ROWS = 20;
    //count of columns
    window.tetrisScope[this.namespace].COLUMNS = 10;

    //key codes
    //window.tetrisScope[this.namespace].KEY_CODE_LEFT = 37;
    //window.tetrisScope[this.namespace].KEY_CODE_RIGHT = 39;
    //window.tetrisScope[this.namespace].KEY_CODE_DOWN = 40;

    window.tetrisScope[this.namespace].KEY_CODE_LEFT = 97;
    window.tetrisScope[this.namespace].KEY_CODE_RIGHT = 100;
    window.tetrisScope[this.namespace].KEY_CODE_DOWN = 115;

    //window.tetrisScope[this.namespace].KEY_CODE_PAUSE = 19;
    //window.tetrisScope[this.namespace].KEY_CODE_ROTATE = 32;

    window.tetrisScope[this.namespace].KEY_CODE_PAUSE = 32;
    window.tetrisScope[this.namespace].KEY_CODE_ROTATE = 119;

    window.tetrisScope[this.namespace].PAUSED = false;
    window.tetrisScope[this.namespace].GAME_OVER = false;

    window.tetrisScope[this.namespace].INTERVAL = 500;

    //figures
    window.tetrisScope[this.namespace].figures = window.figures;

    //count of figures
    window.tetrisScope[this.namespace].FIGURES_COUNT = window.tetrisScope[this.namespace].figures.length;

    //populate indexes for processing in Angular
    window.tetrisScope[this.namespace].rowIndexes = [];
    window.tetrisScope[this.namespace].columnIndexes = [];
    for (var rowIndex = 0; rowIndex < window.tetrisScope[this.namespace].ROWS; rowIndex++) {
        window.tetrisScope[this.namespace].rowIndexes.push(rowIndex);
    }
    for (var columnIndex = 0; columnIndex < window.tetrisScope[this.namespace].COLUMNS; columnIndex++) {
        window.tetrisScope[this.namespace].columnIndexes.push(columnIndex);
    }

    //values of empty and filled cells
    window.tetrisScope[this.namespace].EMPTY = 0;
    window.tetrisScope[this.namespace].FILLED = 1;

    window.tetrisScope[this.namespace].SCORE = 0;
    window.tetrisScope[this.namespace].LEVEL = 1;

    //figure index map after rotation
    window.tetrisScope[this.namespace].indexMap = {};
    window.tetrisScope[this.namespace].indexMap[0] = 0;
    window.tetrisScope[this.namespace].indexMap[1] = 2;
    window.tetrisScope[this.namespace].indexMap[2] = 1;
    window.tetrisScope[this.namespace].indexMap[3] = 4;
    window.tetrisScope[this.namespace].indexMap[4] = 5;
    window.tetrisScope[this.namespace].indexMap[5] = 6;
    window.tetrisScope[this.namespace].indexMap[6] = 3;
    window.tetrisScope[this.namespace].indexMap[7] = 8;
    window.tetrisScope[this.namespace].indexMap[8] = 9;
    window.tetrisScope[this.namespace].indexMap[9] = 10;
    window.tetrisScope[this.namespace].indexMap[10] = 7;
    window.tetrisScope[this.namespace].indexMap[11] = 12;
    window.tetrisScope[this.namespace].indexMap[12] = 13;
    window.tetrisScope[this.namespace].indexMap[13] = 14;
    window.tetrisScope[this.namespace].indexMap[14] = 11;

    //fill cells with empty value
    window.tetrisScope[this.namespace].cells = [];
    for (var i = 0; i < window.tetrisScope[this.namespace].ROWS; i++) {
        window.tetrisScope[this.namespace].cells[i] = [];
        for (var j = 0; j < window.tetrisScope[this.namespace].COLUMNS; j++) {
            window.tetrisScope[this.namespace].cells[i][j] = window.tetrisScope[this.namespace].EMPTY;
        }
    }

    window.tetrisScope[this.namespace].rowIndex = 0;
    window.tetrisScope[this.namespace].columnIndex = 4;
    
    this.initialize(this.namespace);
};

jQuery.extend(true, TetrisComponent, {
    
    prototype: {
        
        initialize: function(namespace){

            window.tetrisScope[namespace].figureIndex = window.tetrisScope[namespace].getRandomFigureIndex(window.tetrisScope[namespace].FIGURES_COUNT);
            window.tetrisScope[namespace].figure = window.tetrisScope[namespace].figures[window.tetrisScope[namespace].figureIndex];
            window.tetrisScope[namespace].cells = window.tetrisScope[namespace].figure.render(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex);

            window.tetrisScope[namespace].addKeyPressListener(namespace);
             
            setTimeout(this.tetrisProcessor, window.tetrisScope[namespace].INTERVAL, namespace);
        },

        checkLines: function(namespace){

            //count of rows
            window.tetrisScope[namespace].ROWS = 20;
            //count of columns
            window.tetrisScope[namespace].COLUMNS = 10;

            for (var rowIndex = window.tetrisScope[namespace].ROWS - 1; rowIndex > 0; rowIndex--) {
                //check if current row completed
                var rowCompleted = true;
                for (var columnIndex = 0; columnIndex < window.tetrisScope[namespace].COLUMNS; columnIndex++) {
                    if (window.tetrisScope[namespace].cells[rowIndex][columnIndex] != window.tetrisScope[namespace].FILLED) {
                        rowCompleted = false;
                        break;
                    }
                }
                if (rowCompleted) {

                    //increase score
                    window.tetrisScope[namespace].SCORE += 100;

                    if (window.tetrisScope[namespace].SCORE % 1000 == 0) {
                        //increase level
                        window.tetrisScope[namespace].LEVEL++;
                        //decrease interval
                        window.tetrisScope[namespace].INTERVAL *= 0.8;
                    }

                    //clear row
                    for (var _columnIndex = 0; _columnIndex < window.tetrisScope[namespace].COLUMNS; _columnIndex++) {
                        window.tetrisScope[namespace].cells[rowIndex][_columnIndex] = window.tetrisScope[namespace].EMPTY;
                    }
                    //move down figures above
                    for (var _rowIndex = rowIndex; _rowIndex > 0; _rowIndex--) {
                        for (var __columnIndex = 0; __columnIndex < window.tetrisScope[namespace].COLUMNS; __columnIndex++) {
                            if (_rowIndex > 0) {
                                window.tetrisScope[namespace].cells[_rowIndex][__columnIndex] = window.tetrisScope[namespace].cells[_rowIndex - 1][__columnIndex];
                            }
                        }
                    }
                    rowIndex++;
                }
            }

            //display changes
            window.tetrisScope[namespace].render(namespace);
        },


        getRandomFigureIndex: function(figuresCount){
            return Math.floor(Math.random() * (figuresCount));  
        },

        moveLeft: function(namespace){
            if (window.tetrisScope[namespace].figure.canMoveLeft(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex)) {
                window.tetrisScope[namespace].cells = window.tetrisScope[namespace].figure.moveLeft(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex);
                window.tetrisScope[namespace].columnIndex--;
            }
        },

        moveRight: function(namespace){
            if (window.tetrisScope[namespace].figure.canMoveRight(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex)) {
                window.tetrisScope[namespace].cells = window.tetrisScope[namespace].figure.moveRight(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex);
                window.tetrisScope[namespace].columnIndex++;
            }
        },

        moveDown: function(namespace){
            if (window.tetrisScope[namespace].figure.canMoveDown(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex)) {
                window.tetrisScope[namespace].cells = window.tetrisScope[namespace].figure.moveDown(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex);
                window.tetrisScope[namespace].rowIndex++;
            }
        },

        rotate: function(namespace){
            if (window.tetrisScope[namespace].figure.canRotate(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex)) {
                //rotate
                window.tetrisScope[namespace].cells = window.tetrisScope[namespace].figure.rotate(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex);
                //change figure after rotate
                window.tetrisScope[namespace].figureIndex = window.tetrisScope[namespace].indexMap[window.tetrisScope[namespace].figureIndex];
                window.tetrisScope[namespace].figure = window.tetrisScope[namespace].figures[window.tetrisScope[namespace].figureIndex];
            }
        },


        pause: function(namespace){
            window.tetrisScope[namespace].PAUSED = !window.tetrisScope[namespace].PAUSED;
        },
        
        
        render: function(namespace){
            //display changes
            var templateFile = "/tetris-portlet/templates/tetris.html";
            jQuery.get(templateFile).done(function(response){
                var template = Handlebars.compile(response);
                var html = template({
                    cells: window.tetrisScope[namespace].cells,
                    level: window.tetrisScope[namespace].LEVEL,
                    score: window.tetrisScope[namespace].SCORE
                });
                window.tetrisScope[namespace].container.html(html);
            }).fail(function(response){
                console.log("Error - response: " + JSON.stringify(response));
            });
        },
        
        tetrisProcessor: function(namespace) {

            if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {

                if (window.tetrisScope[namespace].figure.canMoveDown(
                        window.tetrisScope[namespace].cells, 
                        window.tetrisScope[namespace].rowIndex, 
                        window.tetrisScope[namespace].columnIndex)) {

                    //move down
                    window.tetrisScope[namespace].cells = window.tetrisScope[namespace].figure.moveDown(
                        window.tetrisScope[namespace].cells, 
                        window.tetrisScope[namespace].rowIndex, 
                        window.tetrisScope[namespace].columnIndex
                    );

                    //increment row index
                    window.tetrisScope[namespace].rowIndex++;

                    //display changes
                    window.tetrisScope[namespace].render(namespace);

                } else {

                    //check lines
                    window.tetrisScope[namespace].checkLines(namespace);

                    //new figure
                    window.tetrisScope[namespace].figureIndex = window.tetrisScope[namespace].getRandomFigureIndex(window.tetrisScope[namespace].FIGURES_COUNT);
                    window.tetrisScope[namespace].figure = window.tetrisScope[namespace].figures[window.tetrisScope[namespace].figureIndex];
                    //reset indexes
                    window.tetrisScope[namespace].rowIndex = 0;
                    window.tetrisScope[namespace].columnIndex = 4;
                    //check if figure can be rendered
                    if (window.tetrisScope[namespace].figure.canRender(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex)) {
                        //render figure
                        window.tetrisScope[namespace].cells = window.tetrisScope[namespace].figure.render(window.tetrisScope[namespace].cells, window.tetrisScope[namespace].rowIndex, window.tetrisScope[namespace].columnIndex);
                    } else {
                        //game over
                        var $messageContainer = jQuery("#" + namespace + "message-box");
                        var $messageBox = jQuery("<div class='portlet-msg-error'>You loose!!! Try again.</div>");
                        $messageBox.appendTo($messageContainer);
                        window.tetrisScope[namespace].GAME_OVER = true;
                    }

                }

            }

            setTimeout(window.tetrisScope[namespace].tetrisProcessor, window.tetrisScope[namespace].INTERVAL, namespace);
        },

        
        addKeyPressListener: function (namespace) {

            window.tetrisScope[namespace].container.on("keypress", function ($event) {
                var keyCode = $event.keyCode;
                if (keyCode == 0) {
                    /*For FireFox*/
                    keyCode = $event.charCode;
                }
                
                switch (keyCode) {
                    case window.tetrisScope[namespace].KEY_CODE_LEFT:
                    {
                        if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {
                            window.tetrisScope[namespace].moveLeft(namespace);
                        }
                    }
                        break;
                    case 1092:
                    {
                        if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {
                            window.tetrisScope[namespace].moveLeft(namespace);
                        }
                    }
                        break;
                    case window.tetrisScope[namespace].KEY_CODE_RIGHT:
                    {
                        if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {
                            window.tetrisScope[namespace].moveRight(namespace);
                        }
                    }
                        break;
                    case 1074:
                    {
                        if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {
                            window.tetrisScope[namespace].moveRight(namespace);
                        }
                    }
                        break;
                    case window.tetrisScope[namespace].KEY_CODE_DOWN:
                    {
                        if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {
                            window.tetrisScope[namespace].moveDown(namespace);
                        }
                    }
                        break;
                    case 1099:
                    {
                        if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {
                            window.tetrisScope[namespace].moveDown(namespace);
                        }
                    }
                        break;
                    case window.tetrisScope[namespace].KEY_CODE_ROTATE:
                    {
                        if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {
                            window.tetrisScope[namespace].rotate(namespace);
                        }
                    }
                        break;
                    case 1094:
                    {
                        if (!window.tetrisScope[namespace].PAUSED && !window.tetrisScope[namespace].GAME_OVER) {
                            window.tetrisScope[namespace].rotate(namespace);
                        }
                    }
                        break;
                    case window.tetrisScope[namespace].KEY_CODE_PAUSE:
                    {
                        window.tetrisScope[namespace].pause(namespace);
                    }
                        break;
                }
                //display changes
                window.tetrisScope[namespace].render(namespace);
            });
            
        }
      
    }
    
});

window.TetrisComponent = TetrisComponent;